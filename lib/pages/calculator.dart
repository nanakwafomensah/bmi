import 'package:flutter/material.dart';
import 'package:bmi_app/pages/adult.dart';
import 'package:bmi_app/pages/child.dart';


class CalculatorPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 10.0),
      child: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.deepOrange,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              title: Text('Calculator', style: TextStyle(fontSize: 15.0)),
              bottom: TabBar(
                indicatorColor: Colors.white,
                tabs: [
                  Text(
                    "Adult",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "Child",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ],
              ),
            ),
            body: TabBarView(children: [AdultPage(), ChildPage()]),
          )),
    );
  }
}
