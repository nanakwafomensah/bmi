import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:bmi_app/pages/home.dart';


class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 10,
      navigateAfterSeconds: Home(),
      title: Text('Ultimate BMI Assistant',
        style: TextStyle(
          color: Colors.deepOrange,
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ),
      ),
     image: Image.asset('images/logo.PNG'),
      backgroundColor:Colors.white,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 60.0,
      loaderColor: Colors.deepOrange
    );
  }
}

