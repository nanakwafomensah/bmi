import 'package:flutter/material.dart';
import 'package:bmi_app/pages/calculator.dart';
import 'package:bmi_app/pages/information.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(

          appBar: AppBar(
            backgroundColor: Colors.deepOrange,
            centerTitle: true,
            title: Text('BMI', style: TextStyle(fontSize: 15.0)),
            bottom: TabBar(
              indicatorColor: Colors.white,

              tabs: [
                Tab(icon: Icon(Icons.dialpad)),
                Tab(icon: Icon(Icons.info)),
              ],
            ),
          ),
          body: TabBarView(
            children: [CalculatorPage(), InformationPage()],
          ),
        ));
  }
}

