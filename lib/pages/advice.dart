import 'package:flutter/material.dart';

class Advice extends StatelessWidget {
  String message;

  Advice(this.message);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        margin: EdgeInsets.all(20),
        width: 250,
        height: 200,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          color: Colors.green,
          elevation: 10.0,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Text('Advice:',style: TextStyle(color: Colors.white,fontSize: 16),),

                Flexible(child: Text('$message',style: TextStyle(color: Colors.white,fontSize: 16),)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
