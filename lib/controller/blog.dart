import 'package:bmi_app/models/Bmi.dart';
import 'dart:math';
class Blog {


  static double calculate(BMI bmiValue) {
    double kg = bmiValue.kg;
    double feet = bmiValue.feet;
    double inche = bmiValue.inche;
    double stone = bmiValue.stone;
    double pounds = bmiValue.pounds;
    double cm = bmiValue.cm;

    if (cm == 0.0) {
      cm = feet * 30.48 + inche * 2.54;
    }

    if (kg == 0.0) {
      kg = stone * 6.35029 + pounds * 0.453592;
    }
    return kg / pow(cm * 0.01, 2);
  }
}