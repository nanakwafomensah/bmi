import 'package:flutter/material.dart';
import 'package:bmi_app/models/Bmi.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:bmi_app/pages/advice.dart';
import 'package:bmi_app/controller/blog.dart';

class Result {
  var _resultsMeaning = ['Underweight', 'Normal weight', 'Overweight', 'Obese'];
  var _resultAdvice = ['Set yourself a realistic target and aim to gain weight gradually at around 2.2lbs per month.Make sure you eat at least 3 times per day, including breakfast.',
  'Always eat healthy',
  'Speak to your doctor about a weight loss program that is right for you.',
  'Speak to your doctor about a weight loss program that is right for you.'];

  _getBmiStatus(double computedbmiValue) {
    if (computedbmiValue < 18.5) {
      return _resultsMeaning[0] + '|' + _resultAdvice[0];
    }
    if (computedbmiValue > 18.5 && computedbmiValue < 24.9) {
      return _resultsMeaning[1] + '|' + _resultAdvice[1];
    }
    if (computedbmiValue > 25 && computedbmiValue < 29.9) {
      return _resultsMeaning[2] + '|' + _resultAdvice[2];
    }
    if (computedbmiValue > 30.0) {
      return _resultsMeaning[3] + '|' + _resultAdvice[3];
    }
  }

  resultsView(BuildContext context, BMI bmiValue) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(children: <Widget>[
              Center(
                  child: Text(
                'Your BMI index: ${Blog.calculate(bmiValue).toStringAsFixed(1)}',
                style: TextStyle(fontSize: 15.0),
              )),
              Container(
                  child: SfRadialGauge(axes: <RadialAxis>[
                RadialAxis(minimum: 0, maximum: 70, ranges: <GaugeRange>[
                  GaugeRange(
                      startValue: 0, endValue: 18.5, color: Colors.blueAccent),
                  GaugeRange(
                      startValue: 18.5,
                      endValue: 24.9,
                      color: Colors.greenAccent),
                  GaugeRange(
                      startValue: 25,
                      endValue: 29.9,
                      color: Colors.yellowAccent),
                  GaugeRange(
                      startValue: 30, endValue: 70, color: Colors.redAccent),
                ], pointers: <GaugePointer>[
                  NeedlePointer(
                    value: double.parse(
                        Blog.calculate(bmiValue).toStringAsFixed(1)),
                    enableAnimation: true,
                  )
                ], annotations: <GaugeAnnotation>[
                  GaugeAnnotation(
                      widget: Container(
                          child: Text(
                              '${Blog.calculate(bmiValue).toStringAsFixed(1)}',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold))),
                      angle: 90,
                      positionFactor: 0.5)
                ])
              ])),
              Center(
                  child: Text(
                '${_getBmiStatus(double.parse(Blog.calculate(bmiValue).toStringAsFixed(1))).split('|')[0]}',
                style: TextStyle(fontSize: 15.0),
              )),
              Advice(_getBmiStatus(double.parse(Blog.calculate(bmiValue).toStringAsFixed(1))).split('|')[1])
            ]),
          );
        });
  }
}
